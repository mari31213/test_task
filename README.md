# Комментарии к заданию 2

Для хранения дерева был выбран шаблон `Nested Sets` из-за скорости выборки, в результате конечно теряем 
в производительности при модификации дерева, но так как сайт интернет магазина предполагает редактирование дерева 
категорий только на стороне администатора, то этим фактором можно пренебречь

Так же как вариант рассматривался `Nested Interval`, он имеет лучшую производительность при модификации, но все же 
уступает `Nested Sets` при выборке, пусть и на десятые милисекудны. И так как все таки в условиях было оптимизация 
запросов по выборке, то решение было принято в пользу `Nested Sets`.

Выборка родителей это единственный момент по которому оба этих шаблона уступают `Materialized Path`, но так как только 
один из пяти запросов предполагал это, а в остальных случаях Materialized Path уступает в поисках узлов


На счет оптимальности запросов

То что в идеале надо было поменять, это не использовать подобное выражение `SELECT * FROM ...`, но так как в условиях 
задания точно не сказано какие именно роля необходимо было, я пока оставила так

А так, все задачи были написаны в один запрос, чтобы избежать лишней пересылки данных с PHP к базе.

В задании 2.3.с присутвуют сразу несколько `JOIN`, но альтернатива такому подходу это запрос внутри цыкла, 
что все равно существенно менее производительно. 

В 2.3.d была использована констукция `SELECT COUNT(id) FROM product WHERE` так как лучше суммировать в самом запросе, 
а не выполнить запрос `SELECT * FROM ...` а потом уже подсчитать количество элементов массива полученого в результате 
запроса

В остальных заданиях простые запросы обусловленные выбранным шаблоном проектирования базы данных 

Правда сейчас я уже заметила что выбор потомков не совсем оптимальный. 
Из двух вариантов: 
`node.rgt <= parent.rgt AND node.lft >= parent.lft` и `node.lft <= parent.rgt AND node.lft >= parent.lft` 
второй является более предпочтителен

# Комментарии к заданию 3

В результате решение данной задачи удалось свести к одному вопросу, для получения сразу всей необходимой информации.
Был вариант вначале получить все продукты, и уже потом к ним прибавлять информацию с моделей категории и тега, что
возможно выглядело бы аккуратнее с точки зрения структуры, но в таком случае загрузка страницы с товарами 
увеличивалась в 2-3 раз по времени

Так же чтобы из-за большого количества JOIN при выборке всех родителей категорий у которых есть товары, сразу 
производилась конкатенация всех элементов пути, для того чтобы в конце запроса не так сильно увеличивалось количество 
записей, подобное было сделано и с тегами

Вообще возможно имеет смысл "склеивать" категории уже после того как получили все возможные пути по всем категориям к 
которым присвоен товар, но опять таки, к этому моменту уже может очень сильно разрасти количество записей, и это 
исключено в случае если при отображении необходимо будет восстановить пути по категориям.

У идеи с конкатенацией категорий и тегов есть недостаток в виде ограничений по длинне строки, но так как в задании 
было сказано про 4 уровня вложенности и до 20 тегов, то мы вряд ли выйдем за ограничение. Но такой подход дал нам 
возможность получить одним запросом все товары без необходимости как либо преобразовывать полученый массив на стороне 
PHP, за исключением просто разбития строки на массив категорий, там я фильтровала полученный массив на уникальность, 
от этого можно было бы уйти внедрив изменения которые были описаны в предыдущем пункте. 
Вся фильтрация происходит исключительно на стороне базы данных, что дает нам преимущество в производительности, 
так как бд более оптимизированны под такие задачи
