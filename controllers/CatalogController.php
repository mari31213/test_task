<?php

namespace app\controllers;

use app\models\Product;
use app\models\Tag;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use \yii\helpers\Url;
use app\models\Category;

class CatalogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()    {
    }
    /**
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(Url::to(['user/login'],302));
        };
        return $this->render('index',
            ['products' => Product::getAll()]);
    }


}
