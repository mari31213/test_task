<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use \yii\helpers\Url;
use \yii\web\Cookie;

class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $cookies = Yii::$app->request->cookies;
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && !$cookies->getValue('loginBlocked', 0)) {
            $responseCookies = Yii::$app->response->cookies;
            if($model->login()){
                $failAttempt = $cookies->getValue('failAttempt', 0);
                if($failAttempt){
                    $responseCookies->remove('failAttempt');
                }
                return Yii::$app->getResponse()->redirect(Url::to(['/success']));
            } else {
                $failAttempt = $cookies->getValue('failAttempt', 0);
                if($failAttempt == 2){
                    $blocked = time() + 300;
                    $responseCookies->add(new Cookie([
                        'name' => 'loginBlocked',
                        'value' => $blocked,
                        'expire' => $blocked,
                    ]));
                    $responseCookies->remove('failAttempt');
                } else {
                    $responseCookies->add(new Cookie([
                        'name' => 'failAttempt',
                        'value' => ++$failAttempt,
                    ]));
                    return Yii::$app->getResponse()->redirect(Url::to(['/fail']));

                }
            }
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
            'blocked' => $cookies->getValue('loginBlocked', 0)
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return Yii::$app->getResponse()->redirect(Url::to(['user/login']));
    }

    /**
     * Displays success page after login.
     *
     * @return string
     */
    public function actionSuccess()
    {
        if (Yii::$app->user->isGuest) {
            return Yii::$app->getResponse()->redirect(Url::to(['user/login']));
        }
        return $this->render('success');
    }
    /**
     * Displays fail page after login.
     *
     * @return string
     */
    public function actionFail()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }


        return $this->render('fail');
    }
}
