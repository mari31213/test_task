<?php

namespace app\models;

use Yii;
use yii\db\Query;

class Category extends \yii\base\BaseObject
{

    public $id;
    public $title;
    public $lft;
    public $rgt;
    public $count;


    //Задание 2.3.a
    //По списку товаров получить название всех категорий гдеони содержатся
    public static function getCategoryNamesByProducts($products)
    {
        $records = Yii::$app->db
            ->createCommand('SELECT DISTINCT parent.id, parent.title FROM category AS node, category AS parent 
                                    WHERE node.lft BETWEEN parent.lft AND parent.rgt AND node.id IN 
                                      ( SELECT category_id FROM category_connection WHERE product_id IN (:products) ) 
                                    ORDER BY parent.lft')
            ->bindValue(':products', implode(', ', $products))
            ->queryAll();

        return $records ? $records : [];
    }


    //Задание 2.3.b
    //Получение товаров из категории и ее дочерних категорий
    public static function getProductsByCategory($categoryID)
    {
        $records = Yii::$app->db->createCommand('SELECT * FROM product WHERE id IN  
                (SELECT product_id FROM category_connection WHERE category_id IN (
                    SELECT node.id FROM category AS node, category AS parent 
                    WHERE node.rgt <= parent.rgt AND node.lft >= parent.lft AND parent.rgt AND parent.id=:category
                ))')
            ->bindValue(':category', $categoryID)
            ->queryAll();

        return $records ? array_map(function ($product) {
            return new Product($product);
        }, $records) : [];
    }


    //Задание 2.3.с
    //Для списка категорий получить количество товаров в каждой
    public static function getProductsCount($categories)
    {
        return $category = Yii::$app->db->createCommand(
            'SELECT category.title AS category_title, category.id AS category_id, SUM(connected_table.count) AS count  
                      FROM category LEFT JOIN 
                              (SELECT product.count AS count, category_connection.category_id AS category_id 
                                FROM category_connection 
                                  LEFT JOIN product ON category_connection.product_id = product.id ) AS connected_table 
                       ON connected_table.category_id = category.id WHERE category.id IN 
                                    ( SELECT DISTINCT node.id FROM category AS node, category AS parent 
                                    WHERE node.rgt <= parent.rgt AND node.lft >= parent.lft AND parent.rgt AND parent.id IN (:categories)
                      ) GROUP BY category_id')
            ->bindValue(':categories', implode(', ', $categories))->queryAll();
    }

    //Задание 2.3.d
    //Для списка категорий получить количество товаров в каждой
    // С этим запросом у меня есть вопрос, выполнение запроса в таблице и здесь, с одними и теми же параметрами отличаются результатами
    public static function getPositionCount($categories)
    {
        $count = Yii::$app->db->createCommand('SELECT COUNT(id) FROM product WHERE product.id IN  
                (SELECT product_id FROM category_connection WHERE category_id IN (
                    SELECT node.id FROM category AS node, category AS parent WHERE node.rgt <= parent.rgt AND node.lft >= parent.lft AND parent.rgt AND parent.id IN(:categories)
                ))')
            ->bindValue(':categories', implode(', ', $categories))
            ->queryScalar();
        return $count ? $count : 0;

    }


    //Задание 2.3.e
    //Получить полный путь по дереву для категории
    public static function findPath($categoryID)
    {
        $records = Yii::$app->db
            ->createCommand('SELECT * FROM category AS node, category AS parent 
                                      WHERE node.lft BETWEEN  parent.lft AND parent.rgt AND node.id=:category
                                      ORDER BY parent.lft')
            ->bindValue(':category', $categoryID)
            ->queryAll();

        return $records ? array_map(function ($category) {
            return new static($category);
        }, $records) : [];
    }


}