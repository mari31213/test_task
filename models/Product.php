<?php

namespace app\models;
use \yii\db\Query;
use Yii;

class Product extends \yii\base\BaseObject
{
    public $id;
    public $title;
    public $price;
    public $count;
    public $categories;
    public $tags;


    /**
     * {@inheritdoc}
     */
    public static function getByID($id)
    {
        $record = (new Query())
            ->from('product')
            ->where(['id' => $id])
            ->one();
        return $record ? new static($record) : null;
    }

    public static function getAll(){
        $records = Yii::$app->db
            ->createCommand('
            SELECT DISTINCT product.id, product.title, product.price, product.count, GROUP_CONCAT(DISTINCT connected_table.category_title SEPARATOR "|") AS categories, tags.path as tags 
            FROM product 
            LEFT JOIN ( SELECT category.path AS category_title, product_id 
                        FROM category_connection 
                        LEFT JOIN ( 
                            SELECT node.id as id, GROUP_CONCAT(DISTINCT parent.title SEPARATOR "|") AS path 
                            FROM category AS node, category AS parent 
                            WHERE node.rgt <= parent.rgt AND node.lft >= parent.lft AND parent.rgt  AND node.id IN (SELECT DISTINCT category_id FROM category_connection)
                            GROUP BY node.id ) AS category 
                        ON category.id=category_connection.category_id) 
            AS connected_table ON connected_table.product_id=product.id 
            LEFT JOIN ( 
                        SELECT product_id as product_id, GROUP_CONCAT(DISTINCT tag.title SEPARATOR "|") AS path 
                        FROM tag_connection 
                        LEFT JOIN tag 
                        ON tag.id=tag_connection.tag_id 
                        GROUP BY product_id) 
            AS tags 
            ON tags.product_id=product.id
            GROUP BY product.id')
            ->queryAll();

        return $records ? array_map(function ($product) {
            return new static($product);
        }, $records) : [];
    }

}
