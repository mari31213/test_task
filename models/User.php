<?php

namespace app\models;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
    ];

    public static function getUsers(){
        if (($handle = fopen(realpath(__DIR__ . '/users.csv'), "r")) !== FALSE) {
            $headerObj = [];
            $headerIsRead = 0;
            while (($data = fgetcsv($handle, 2000, ",")) !== FALSE) {
                if ($headerIsRead == 0) {
                    $headerObj = array_map(function ($str){return trim(mb_convert_encoding($str, 'UTF-8'), ' ');}, $data);;
                    $headerIsRead = 1;
                    continue;
                }
                $form = [];
                foreach ($data as $key => $value) {
                    $form[$headerObj[$key]] = addslashes(trim(mb_convert_encoding($value, 'UTF-8'), ' '));
                }
                self::$users[$form['id']] = $form;
            }
            fclose($handle);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        if(empty(self::$users)){
            self::getUsers();
        }
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        if(empty(self::$users)){
            self::getUsers();
        }
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        if(empty(self::$users)){
            self::getUsers();
        }
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return password_verify($password, $this->password);
    }
}
