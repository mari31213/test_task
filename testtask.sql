-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 10 2018 г., 18:20
-- Версия сервера: 5.6.41
-- Версия PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testtask`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `title`, `lft`, `rgt`) VALUES
(1, 'ELECTRONICS', 1, 20),
(2, 'TELEVISIONS', 2, 9),
(3, 'TUBE', 3, 4),
(4, 'LCD', 5, 6),
(5, 'PLASMA', 7, 8),
(6, 'PORTABLE ELECTRONICS', 10, 19),
(7, 'MP3', 11, 14),
(8, 'FLASH', 12, 13),
(9, 'CD PLAYERS', 15, 16),
(10, '2 WAYS RADIO', 17, 18);

-- --------------------------------------------------------

--
-- Структура таблицы `category_connection`
--

CREATE TABLE `category_connection` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category_connection`
--

INSERT INTO `category_connection` (`id`, `category_id`, `product_id`) VALUES
(1, 3, 1),
(3, 3, 2),
(4, 4, 5),
(5, 4, 3),
(6, 5, 4),
(7, 8, 5),
(8, 10, 6),
(9, 5, 7),
(10, 4, 13),
(11, 5, 14),
(12, 8, 15),
(13, 10, 16),
(14, 5, 17),
(15, 4, 12),
(16, 5, 11),
(17, 8, 10),
(18, 10, 9),
(19, 5, 8),
(20, 4, 18),
(21, 5, 19),
(22, 8, 20),
(23, 10, 21),
(24, 5, 22),
(25, 4, 23),
(26, 5, 24),
(27, 8, 25),
(28, 26, 9),
(29, 27, 8),
(30, 4, 28),
(31, 5, 29),
(32, 8, 30),
(33, 10, 31),
(34, 5, 32),
(35, 4, 33),
(36, 5, 34),
(37, 8, 35),
(38, 7, 36),
(39, 7, 37),
(40, 4, 38),
(41, 5, 39),
(42, 8, 40),
(43, 10, 41),
(44, 5, 42),
(45, 4, 43),
(46, 5, 44),
(47, 8, 45),
(48, 7, 46),
(49, 7, 47),
(50, 4, 48),
(51, 5, 49),
(52, 8, 50),
(53, 10, 51),
(54, 5, 52),
(55, 4, 53),
(56, 5, 54),
(57, 8, 55),
(58, 7, 56),
(59, 7, 57),
(60, 4, 58),
(61, 5, 59),
(62, 8, 60),
(63, 10, 61),
(64, 5, 62),
(65, 4, 63),
(66, 5, 64),
(67, 8, 65),
(68, 7, 66),
(69, 7, 67),
(70, 4, 68),
(71, 5, 69),
(72, 8, 70),
(73, 10, 71),
(74, 5, 72),
(75, 4, 73),
(76, 5, 74),
(77, 8, 75),
(78, 7, 76),
(79, 7, 77),
(80, 4, 78),
(81, 5, 79),
(82, 8, 80),
(83, 10, 81),
(84, 5, 82),
(85, 4, 83),
(86, 5, 84),
(87, 8, 85),
(88, 7, 86),
(89, 7, 87),
(90, 4, 88),
(91, 5, 89),
(92, 8, 90),
(93, 10, 91),
(94, 5, 92),
(95, 4, 93),
(96, 5, 94),
(97, 8, 95),
(98, 7, 96),
(99, 7, 97),
(100, 7, 1),
(101, 9, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `title` char(50) NOT NULL,
  `price` decimal(50,2) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `title`, `price`, `count`) VALUES
(1, 'ipod nano 6', '3.00', 5),
(2, 'some phone', '70.00', 2),
(5, 'charger', '10.00', 3),
(6, 'electric sc', '150.00', 4),
(7, 'usb', '10.00', 1),
(8, 'headphones', '12.00', 4),
(9, 'gear sport', '122.00', 0),
(10, 'xbox', '60.00', 1),
(11, 'ps4', '70.00', 12),
(12, 'gamepad', '11.00', 7),
(13, 'one more tv', '10.00', 5),
(14, 'not a tv', '15.00', 4),
(15, 'sony tv', '10.00', 4),
(16, 'samsung tv', '3.00', 1),
(17, 'urbanears h', '1.00', 1),
(18, 'imagination problem', '99999.00', 4),
(19, 'one more test record', '10.99', 4),
(20, 'i need 80 more records', '13.30', 1),
(21, '\r\ncan i just copy the previous ones', '10.00', 4),
(22, 'or it\'s a cheating', '10.00', 4),
(23, 'test 2', '2.00', 1204),
(24, 'one more gamepad', '10.00', 1),
(25, 'icecream', '13.00', 3),
(26, 'whiteboar', '1.00', 15),
(27, 'water', '5.00', 1),
(28, 'plant', '5.50', 4),
(29, 'glass', '5.75', 3),
(30, 'hicking shoes', '25.00', 4),
(31, 'tv', '10.00', 546),
(32, 'i need some sleep', '10.00', 9999),
(33, 'but i can\'t', '10.00', 4),
(34, 'i need to finish my test task', '10.00', 17),
(35, 'hard drive', '10.00', 4),
(36, 'mikrotik', '50.00', 7),
(37, 'printer', '10.00', 2),
(38, 'glass protector', '12.00', 2),
(39, 'case', '10.00', 3),
(40, 'one more plant', '10.00', 1),
(41, 'realy big problems with imagination', '99.99', 4),
(42, 'tv', '10.00', 1),
(43, 'not a tv at all', '13.00', 4),
(44, 'water bottle', '11.00', 5),
(45, 'is\'t important to make different title', '13.00', 5),
(46, 'glass', '10.00', 5),
(47, 'tv', '10.40', 5),
(48, 'one more charger', '2.00', 48),
(49, 'tv', '10.00', 100),
(50, 'glass protector', '111.11', 4),
(51, 'tv', '9.90', 7),
(52, 'test 3', '10.00', 4),
(53, 'tv', '10.00', 4),
(54, 'tv', '10.00', 13),
(55, 'tv', '10.00', 4),
(56, 'tv', '10.00', 1),
(57, 'tv', '10.00', 1),
(58, 'tv', '10.00', 4),
(59, 'tv', '10.00', 1),
(60, 'tv', '10.00', 0),
(61, 'tv', '10.00', 0),
(62, 'tv', '10.00', 0),
(63, 'tv', '10.00', 0),
(64, 'tv', '10.00', 5),
(65, 'tv', '10.00', 4),
(66, 'tv', '10.00', 6),
(67, 'tv', '10.00', 4),
(68, 'tv', '10.00', 4),
(69, 'tv', '10.00', 657),
(70, 'tv', '10.00', 4324),
(71, 'tv', '10.00', 3342),
(72, 'tv', '10.00', 54),
(73, 'tv', '10.00', 42),
(74, 'tv', '10.00', 4),
(75, 'tv', '10.00', 4324),
(76, 'tv', '10.00', 12),
(77, 'tv', '10.00', 7),
(78, 'tv', '10.00', 43),
(79, 'tv', '10.00', 2),
(80, 'tv', '10.00', 4),
(81, 'tv', '10.00', 416),
(82, 'tv', '10.00', 4),
(83, 'tv', '10.00', 71),
(84, 'tv', '10.00', 4),
(85, 'tv', '10.00', 15),
(86, 'tv', '10.00', 4),
(87, 'tv', '10.00', 42),
(88, 'tv', '10.00', 234),
(89, 'tv', '10.00', 4),
(90, 'tv', '10.00', 5),
(91, 'tv', '10.00', 5),
(92, 'tv', '10.00', 4),
(93, 'tv', '10.00', 0),
(94, 'tv', '10.00', 4),
(95, 'tv', '10.00', 7),
(96, 'tv', '10.00', 8),
(97, 'tv', '10.00', 4),
(98, 'tv', '10.00', 1),
(99, 'tv', '10.00', 42),
(100, 'tv', '10.00', 2),
(101, 'tv', '10.00', 2),
(102, 'tv', '10.00', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tag`
--

INSERT INTO `tag` (`id`, `title`) VALUES
(1, 'TV'),
(2, 'OLED'),
(3, 'mp3'),
(4, 'CD'),
(5, 'Samsung'),
(6, 'portable'),
(7, 'just a tag'),
(8, 'tag'),
(9, 'and one more thing'),
(10, 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `tag_connection`
--

CREATE TABLE `tag_connection` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tag_connection`
--

INSERT INTO `tag_connection` (`id`, `tag_id`, `product_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 4, 1),
(4, 4, 1),
(5, 5, 1),
(6, 4, 2),
(7, 3, 1),
(8, 6, 2),
(9, 3, 2),
(10, 7, 1),
(11, 7, 2),
(12, 8, 15),
(13, 10, 16),
(14, 5, 17),
(15, 4, 12),
(16, 5, 11),
(17, 8, 10),
(18, 10, 9),
(19, 5, 8),
(20, 4, 18),
(21, 5, 19),
(22, 8, 20),
(23, 10, 21),
(24, 5, 22),
(25, 4, 23),
(26, 5, 24),
(27, 8, 25),
(28, 26, 9),
(29, 27, 8),
(30, 4, 28),
(31, 5, 29),
(32, 8, 30),
(33, 10, 31),
(34, 5, 32),
(35, 4, 33),
(36, 5, 34),
(37, 8, 35),
(38, 7, 36),
(39, 7, 37),
(40, 4, 38),
(41, 5, 39),
(42, 8, 40),
(43, 10, 41),
(44, 5, 42),
(45, 4, 43),
(46, 5, 44),
(47, 8, 45),
(48, 7, 46),
(49, 7, 47),
(50, 4, 48),
(51, 5, 49),
(52, 8, 50),
(53, 10, 51),
(54, 5, 52),
(55, 4, 53),
(56, 5, 54),
(57, 8, 55),
(58, 7, 56),
(59, 7, 57),
(60, 4, 58),
(61, 5, 59),
(62, 8, 60),
(63, 10, 61),
(64, 5, 62),
(65, 4, 63),
(66, 5, 64),
(67, 8, 65),
(68, 7, 66),
(69, 7, 67),
(70, 4, 68),
(71, 5, 69),
(72, 8, 70),
(73, 10, 71),
(74, 5, 72),
(75, 4, 73),
(76, 5, 74),
(77, 8, 75),
(78, 7, 76),
(79, 7, 77),
(80, 4, 78),
(81, 5, 79),
(82, 8, 80),
(83, 10, 81),
(84, 5, 82),
(85, 4, 83),
(86, 5, 84),
(87, 8, 85),
(88, 7, 86),
(89, 7, 87),
(90, 4, 88),
(91, 5, 89),
(92, 8, 90),
(93, 10, 91),
(94, 5, 92),
(95, 4, 93),
(96, 5, 94),
(97, 8, 95),
(98, 7, 96),
(99, 7, 97);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `authKey` text,
  `accessToken` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `authKey`, `accessToken`) VALUES
(1, 'admin', '$2y$10$B0lJroTdQOWNYZmASIo3t.dxcOFhxNTWIUPQ3C/4cSYDyIApepcLe', NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category_connection`
--
ALTER TABLE `category_connection`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tag_connection`
--
ALTER TABLE `tag_connection`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `category_connection`
--
ALTER TABLE `category_connection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT для таблицы `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `tag_connection`
--
ALTER TABLE `tag_connection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
