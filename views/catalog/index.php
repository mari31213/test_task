<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $products */


$this->registerJsFile('@web/js/common.js', ['depends' => 'yii\web\YiiAsset']);

foreach ($products as $product) {
    $categories = '';
    if ($product->categories) {
        foreach (array_unique(explode('|', $product->categories)) as $category) {
            $categories .= "<span class='category'> $category </span>";
        }

    }
    $tags = '';
    if ($product->tags) {
        foreach (explode('|', $product->tags) as $tag) {
            $tags .= "<span class='tag'> $tag </span>";
        }
    }
    printf('<div class="row-fluid">
  <div class="col-lg-12 product-row" >
    <div class="col-lg-6">%s (В наличии <span class="count">%s</span>)</div>
    <div class="col-lg-3 col-lg-offset-3 text-right price"><span>%s</span> KWT</div>
    %s%s
  </div>
</div>',
        $product->title,
        $product->count,
        $product->price,
        $categories ? "<div class='row-fluid'><div class='col-lg-12 text-left'>Категории: $categories</div></div>" : '',
        $tags ? "<div class='row-fluid'><div class='col-lg-12 text-left'>Теги: $tags</div></div>" : ''
    );
};
?>
<style>
    .product-row {
        box-sizing: border-box;
        padding: 20px;
    }

    .container > .row-fluid:nth-child(2n) {
        background-color: #cccccc29;
    }

    .category, .tag {
        font-size: 10px;
        padding: 1px;
        border-radius: 3px;
    }

    .category {
        background-color: #ccfffc;
        border: 1px solid rgba(116, 223, 255, 0.57);
        margin-left: 10px;
    }

    .tag {
        background-color: #ffd4d4;
        border: 1px solid #ff000091;
        margin-left: 10px;
    }
</style>

