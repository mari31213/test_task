<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Fail';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1>Неверные данные</h1>

    <?=
    Html::beginForm(['/login'], 'get') .
    Html::submitButton(
        'Login',
        ['class' => 'btn btn-primary']
    ) .
    Html::endForm()?>

</div>
