<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Success';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1>Добрый день, <?= Html::encode(Yii::$app->user->identity->username) ?></h1>

    <?=
    Html::beginForm(['/logout'], 'post') .
    Html::submitButton(
        'Click to Logout',
        ['class' => 'btn btn-primary']
    ) .
    Html::endForm()?>

</div>
