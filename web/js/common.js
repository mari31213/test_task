var calcProductCount = function (number) {
    if(number === '0'){
        return 'нет';
    }
    var result = '';
    var numbers =  [
        'одна',
        'две',
        'три',
        'четыри',
        'пять',
        'шесть',
        'семь',
        'восемь',
        'девять'
    ];
    var hundred = [
        'сто',
        'двести',
        'триста',
        'четыреста',
        'пятсот',
        'шестсот',
        'семьсот',
        'восемсот',
        'девятсот',
    ];
    var tens = [
        'двадцать',
        'тридцать',
        'сорок',
        'пятьдесят',
        'шестьдесят',
        'семьдесят',
        'восемдесят',
        'девяносто',
    ];
    var length = number.length;
    if(length === 4) {
        var thousand = numbers[parseInt(number[0]) - 1];
        result = `${thousand} ${plural(thousand, 'тысяча', 'тысячи', 'тысячь')} `;
    }
    if(length > 2) {
        result += (hundred[parseInt(number[length-3]) - 1] + ' ');
    }
    if(length > 1){
        if(number.substr(length-2, 2) > 20) {
            result += `${tens[parseInt(number[length-2]) - 2]} ${numbers[parseInt(number[length - 1]) - 1]}`;
        } else {
            switch (number.substr(length-2, 2)) {

                case '00':
                    break;
                case '10':
                    result += 'десять';
                    break;
                case '11':
                    result += 'одинадцать';
                    break;
                case '12':
                    result += 'двенадцать';
                    break;
                case '13':
                    result += 'тринадцать';
                    break;
                case '14':
                    result += 'четырнаднцать';
                    break;
                case '15':
                    result += 'пятнадцать';
                    break;
                case '16':
                    result += 'шестнадцать';
                    break;
                case '17':
                    result += 'семьнадцать';
                    break;
                case '18':
                    result += 'восемнадцать';
                    break;
                case '19':
                    result += 'девятнадцать';
                    break;
                default :
                    result += numbers[parseInt(number[length - 1]) - 1];
                    break;
            }
        }
    } else {
        return numbers[parseInt(number[0]) - 1];
    }
    return result;

};
var plural = function (number, one, two, five) {
    number = parseInt(number);
    return ((number - number % 10) % 100 !== 10) ? ((number % 10 === 1) ? one : ((number % 10 >= 2 && number % 10 <= 4) ? two: five)) : five;
};

$(document).ready(function () {
    $('span.count').each(function () {
        var number = $(this).text();
        if(number.length < 5){
            $(this).text(calcProductCount(number) + ' ' + plural(number, 'единица', 'единицы', 'единиц'));
        }
    });
});
