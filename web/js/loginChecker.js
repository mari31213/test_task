$(document).ready(function () {
    if($('#login-form.blocked')){
        var interval = setInterval(function () {
            var blockedUntil = $('.warning strong').text();
            if(--blockedUntil){
                $('.warning strong').text(blockedUntil);
            } else {
                $('#login-form').removeClass('blocked');
                $('.warning').remove();
                clearInterval(interval);
            }
        }, 1000);
    }
});